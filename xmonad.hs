import           XMonad

import           XMonad.Actions.Navigation2D
import           XMonad.Actions.SpawnOn
import           XMonad.Config
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.SetWMName
import           XMonad.Hooks.StatusBar
import           XMonad.Hooks.StatusBar.PP
import           XMonad.Layout.FixedColumn
import           XMonad.Layout.Grid
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.NoBorders
import           XMonad.Layout.Spacing

import           XMonad.Util.CustomKeys
import           XMonad.Util.Run

import           Data.List

import           MyKeyBindings


main :: IO ()
main = do
    mySB <- statusBarPipe "xmobar" (pure myPP)
    xmonad . withSB mySB . docks $ myConfig

mySB = statusBarProp "xmobar" (pure xmobarPP)
myPP = def
  { ppCurrent = xmobarColor "black" "white"
  , ppHidden = pad . xmobarColor middleColor ""
  , ppHiddenNoWindows = pad . xmobarColor shadedColor ""
  , ppLayout = const ""
  , ppTitle = titleStyle
  , ppVisible = pad . xmobarColor middleColor ""
  , ppWsSep = " "
  }
  where
    titleStyle = xmobarColor titleColor "" . shorten 100 . filterCurly
    filterCurly = filter (not . isCurly)
    isCurly x = x == '{' || x == '}'

backgroundColor   = "#202020"
middleColor       = "#AEAEAE"
shadedColor       = "#606060"
foregroundColor   = "#9a2bc2"
titleColor        = "#2bc26f"

myConfig = def
  { borderWidth        = 4
  , focusedBorderColor = foregroundColor
  , focusFollowsMouse  = False
  , keys               = myKeys
  , layoutHook         = myLayoutHook
  , manageHook         = manageDocks <+> manageSpawn
  , modMask            = mod4Mask
  , normalBorderColor  = middleColor
  , terminal           = "gnome-terminal"
  , workspaces         = [ "Net", "emacs", "Terminal", "Komm", "etc" ]
  , startupHook        = startup
  }

-- with spacing
myLayoutHook = (spacing 10 $ avoidStruts (Full ||| GridRatio (4/3) ||| tall )) ||| smartBorders Full
  where tall = Tall 1 (3/100) (1/2)

startup :: X ()
startup = do
  setWMName "LG3D"
  spawnOn "Net" "firefox"
  spawnOn "Net" "thunderbird"
  spawnOn "emacs" "emacs"
  spawnOn "Komm" "signal-desktop"
  spawn "stalonetray"
  spawn "flameshot"
